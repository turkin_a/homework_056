import React from 'react';
import './Button.css';

const Button = props => {
  return (
    <button
      className={props.className}
      type="button"
      onClick={() => props.click(props.ingredient, props.className)}
    >{props.name}</button>
  );
};

export default Button;