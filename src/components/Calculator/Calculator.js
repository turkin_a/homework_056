import React from 'react';
import './Calculator.css';
import Adjustment from "./Adjustment/Adjustment";

const Calculator = props => {
  let calculatePrice = () => {
    let sum = 0;

    for (let key in props.stuffing) {
      sum += props.stuffing[key].reduce((acc, el) => {return acc + el;}, 0);
    }

    return sum;
  };

  return (
    <div className="Calculator">
      <p className="TotalPrice">Current price: {calculatePrice()} KGS</p>
      <Adjustment
        stuffing={props.stuffing}
        less={props.less}
        more={props.more}
      />
    </div>
  );
};

export default Calculator;