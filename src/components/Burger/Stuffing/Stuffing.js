import React from 'react';

const Stuffing = (props) => {
  let id = 0;

  return Object.keys(props.stuffing).map((stuff) => {
    return props.stuffing[stuff].map(() => {
      id++;
      return (
        <div className={stuff} key={id}/>
      );
    });
  });
};

export default Stuffing;